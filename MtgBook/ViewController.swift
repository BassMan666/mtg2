//
//  ViewController.swift
//  MtgBook
//
//  Created by Etudiant on 16-11-17.
//  Copyright © 2016 BassAnt. All rights reserved.
//
//  Programmé par Bastien Latour-Larocque
//  Designé par Antoine Duperré

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource{
    
    @IBOutlet weak var CVCarte: UICollectionView!
    //private var _listeDesItems =
    private var dic_resultats  = Array<Dictionary<String, Any>>()
    
    
    //création de l'url
    var URLDeckBrew = ""
    let URLp1 = "https://api.deckbrew.com/mtg/cards?"
    var URLPageNb = 1
    //let URLPage = "page=\(URLPageNb)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // while URLPageNb<169{
        var URLPage = "page=\(URLPageNb)"
        URLDeckBrew = URLp1 + URLPage
        //print(URLDeckBrew)
        //obtenirLesDonnees(URLDeckBrew)
        testerJSON()
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.doTimer), userInfo: nil, repeats: true)
        
        //URLPageNb += 1
        // }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doTimer(){
        //obtenirLesDonnees(URLDeckBrew)
        testerJSON()
        afficherCartes()
        ///  Actualiser une collectionView
        ///  tableViewActions.reloadData()
    }
    
    func afficherCartes() {
        print("---------------------------------------")
        for action in dic_resultats{
            if let _image = action["name"] {
               // print("Carte: \(_image)" )
                
                //print("===================================================")
            }
        } // for action in
        //print("---------------------------------------")
    } // afficherActions()
    
    /*func obtenirLesDonnees(_ url:String) {
        let uneURL = URL(string: url)!  //Danger!
        
        /// Exécuter le traitement suivant en parallèle
        DispatchQueue.main.async ( execute: {
            if let _donnees = NSData(contentsOf: uneURL) as? Data {
                do {
                    let json = try JSONSerialization.jsonObject(with: _donnees, options: JSONSerialization.ReadingOptions()) as? Array<Dictionary<String, Any>>
                    
                    print("Conversion JSON réussie")
                    self.dic_resultats = json!
                    //print(self.dic_resultats)
                    // Créer un tableau à partir du champ 'resultats'
                    
                    //print(json)
                } catch {
                    print("\n\n#Erreur: Problème de conversion json:\(error)\n\n")
                } // do/try/catch
            } else
            {
                print("\n\n#Erreur: impossible de lire les données via:\(self.URLDeckBrew)\n\n")
            } // if let _données = NSData
        }) // DispatchQueue.main.async
    }*/
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dic_resultats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var celluleCourante:CVCCartes
        
        celluleCourante = collectionView.dequeueReusableCell(withReuseIdentifier: "modeleCell", for:indexPath) as! CVCCartes
        
        // celluleCourante.imgCarte
        return celluleCourante
    }
    func testerJSON(){
        
        let strURL = URLDeckBrew
        
        /// Exécuter le traitement suivant en parallèle
        if let _donnees = NSData(contentsOf: URL(string: strURL)!) as? Data {
            
            let json = JSON(data: _donnees)
            if let name = json[][]["editions"].array {
                print("le nom de la carte est \(name)")
            }
            
            /*let imagePochette = json[]["editions"]["image_url"].string
            print(imagePochette)*/
            
            /*for item in json[]["editions"].arrayValue {
                print("-------------------------")
                print("Carte: " + item["image_url"].string!)
                

                
            }*/
        }
    }
    
}

