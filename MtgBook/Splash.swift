//
//  Splash.swift
//  MtgBook
//
//  Created by Etudiant on 16-11-17.
//  Copyright © 2016 BassAnt. All rights reserved.
//

import UIKit

class Splash: UIViewController {

   
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        UIView.animate(withDuration: 3.0,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { self.imgView1.center.x = self.imgView1.center.x-500 },
                       completion: nil)
        UIView.animate(withDuration: 3.0,
                       delay: 2.0,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { self.imgView2.center.x = self.imgView2.center.x+600 },
                       completion: nil)
        Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(Splash.passerAuMenuPrincipal), userInfo: nil, repeats: false)
    } // viewDidLoad()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func passerAuMenuPrincipal(){
        performSegue(withIdentifier: "SegSplash", sender: self)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
